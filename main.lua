-- libs
local util = require 'util'
local pushdown = require 'util.pushdown'
local nk = util.nk
ui = require 'ui'

-- base state
globalState = {
    gameWindowW = 1920,
    gameWindowH = 1080,
    ratio = 1920/1080,
    debug = true,
}

-- user game
local game = require 'states.game'

function love.load()
    nk.init()
    globalState.canvas = love.graphics.newCanvas()
    globalState.clickQueue = util.util.Queue()
    local g = game()
    pushdown.push(g)
end

function love.update(dt)
    pushdown.update(dt)

    if globalState.debug then
        nk.frameBeginF(function()
            ui.build()
            -- print(#ui.views)

            nk.windowBeginF(
                {'buffer', 800, 100, 960, 540,
                    'title', 'movable', 'scalable', 'minimizable'},
                function()

                    -- we need 0 padding,
                    -- because getcontentregion gives w and h without padding
                    -- causing the mouse coord to be translated incorrectly
                    nk.stylePush {
                        ['window'] = {
                            ['padding'] = {x = 0, y = 0},
                        }
                    }

                    _, _, globalState.gameWindowW, globalState.gameWindowH = util.nk.windowGetContentRegion()
                    local w, h = util.util.calculateWH(globalState.gameWindowW, globalState.gameWindowH, globalState.ratio)
                    nk.layoutRow('static', h, w, 1)

                    nk.image(globalState.canvas)

                    nk.stylePop()

                    -- handle mouse events,
                    -- see love.mousepressed

                    -- todo make own layout to local fn in utils
                    -- use getcontentregion
                    local scale = w/1920

                    while globalState.clickQueue:peek() do
                        local msg, x, y, a, b, c = unpack(globalState.clickQueue:pop())

                        x, y = nk.layoutSpaceToLocal(x, y)
                        if x >= 0 and x < 1920 and y >= 0 and y < 1080 then
                            if msg == 'mousepressed' then
                                pushdown.mousepressed(x/scale, y/scale, a, b, c) 
                            elseif msg == 'mousemoved' then
                                pushdown.mousemoved(x/scale, y/scale, a/scale, b/scale, c)
                            end
                        end
                    end
                end
            )
        end)
    end
end

function love.draw()
    love.graphics.setCanvas(globalState.canvas)
    love.graphics.clear(1,0,0)

    util.graphics.push(function()
        pushdown.draw()
    end)

    love.graphics.setCanvas()

    if globalState.debug then
        nk.draw()
    else
        love.graphics.setColor(1, 1, 1)
        love.graphics.draw(globalState.canvas)
    end
end

function love.keypressed(key, scancode, isrepeat)
    if nk.keypressed(key, scancode, isrepeat) then return end

    pushdown.keypressed(key, scancode, isrepeat)
end

function love.keyreleased(key, scancode)
    if key == 'escape' then
        love.event.quit()
    end

    if key == 'd' then
        globalState.debug = not globalState.debug
    end

    pushdown.keyreleased(key, scancode)

	nk.keyreleased(key, scancode)
end

function love.mousepressed(x, y, button, istouch)
    nk.mousepressed(x, y, button, istouch)

    if globalState.debug then
        -- when drawing in a window the x,y pos has to be translated to
        -- game coords, but the fn to do that can only be called with
        -- an active nuklear window
        --
        -- to achieve this we push the event to a queue and handle it later
        globalState.clickQueue:push({'mousepressed', x, y, button, istouch})
    else
        pushdown.mousepressed(x, y, button, istouch) 
    end
end

function love.mousereleased(x, y, button, istouch)
    nk.mousereleased(x, y, button, istouch)
    
    -- if game.mousereleased then game.mousereleased(x,y,button,istouch) end
end

function love.mousemoved(...)
    nk.mousemoved(...)

    if globalState.debug then
        -- see mousepressed
        globalState.clickQueue:push({'mousemoved', ...})
    else
        pushdown.mousemoved(...) 
    end

end

function love.textinput(text)
	nk.textinput(text)
end

function love.wheelmoved(x, y)
	nk.wheelmoved(x, y)
end

function love.run()
	if love.load then love.load(love.arg.parseGameArguments(arg), arg) end
 
	-- We don't want the first frame's dt to include time taken by love.load.
	if love.timer then love.timer.step() end
 
	local dt = 0
 
	-- Main loop time.
	return function()
		-- Process events.
		if love.event then
			love.event.pump()
			for name, a,b,c,d,e,f in love.event.poll() do
				if name == "quit" then
					if not love.quit or not love.quit() then
						return a or 0
					end
				end
				love.handlers[name](a,b,c,d,e,f)
			end
		end
 
		-- Update dt, as we'll be passing it to update
		if love.timer then dt = love.timer.step() end
 
		-- Call update and draw
		if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled
 
		if love.graphics and love.graphics.isActive() then
			love.graphics.origin()
			love.graphics.clear(love.graphics.getBackgroundColor())
 
			if love.draw then love.draw() end
 
			love.graphics.present()
		end
 
		if love.timer then love.timer.sleep(0.001) end
	end
end