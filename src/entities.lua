local com = require 'components'
local vector = require 'vector'

local types = {
    player = 'player',
    bullet = 'bullet',
    enemy = 'enemy',
}

local mod = {types = types}
mod.__index = mod

function mod.Base(e, v)
    e = e or com()
    e:Position(v)
    e:Velocity()
    e.r = 50
    e.drag = 0.01

    return e
end

function mod.Player(loc, image)
    e = mod.Base(nil, loc)
    e.type = types.player
    e:Attack()
    e:Direction()
    e:Sprite(image, {collidable = true})
    e:Box(vector(14,14))
    return e
end

function mod.Enemy(v)
    e = mod.Base(nil, v)
    e:DrawData{color = com.Color{0, 0, 1}}
    e.type = types.enemy
    return e
end

function mod.Bullet(p,v)
    e = com()
    e.type = types.bullet
    e:Position(p)
    e:Velocity(v)
    e.r = 20
    e:DrawData{color = com.Color{1,1,1}}
    e:Collision('friendly')

    return e
end

return setmetatable({}, mod)