local vector = require 'vector'
local camera = require 'camera'
local tiny = require 'tiny'
local sti = require 'sti'
local bump = require 'bump' 

local com = require 'components'
local entities = require 'entities'
local systems = require 'systems'
-- local ui = require 'ui'

local game = {
    scale = 2
}
game.__index = game

local function new()
    return setmetatable({}, game)
end

function game:load()

    -- setup the world
    self.world = tiny.world()
    self.world.bump = bump.newWorld(16)

    -- load the map and add the sprites layer
    self.world.map = sti("resources/wispers.lua", {"bump"})
    self.world.map:bump_init(self.world.bump)
    local spriteLayer = self.world.map:addCustomLayer("sprites") 
    spriteLayer.sprites = {}
    function spriteLayer:draw()
        for e, _ in pairs(self.sprites) do
            local w, h = e.sprite.image:getDimensions()
            local pos = e.position - vector(w,h)/2
			love.graphics.draw(e.sprite.image, math.floor(pos.x), math.floor(pos.y))
		end
    end

    ui.register_world(self.world)
    ui.set_detail(self.player)

    -- systems
    self.world:add(
        systems.movement,
        systems.attack,
        systems.draw,
        systems.collision,
        systems.sprite
    )

    local spawn
    for _,n in pairs(self.world.map.layers.points.objects) do
        if n.name == "spawn" then
            spawn = n
        end
    end
    assert(spawn, "map needs player spawn point")
    print(spawn.x, spawn.y)

    local pl_sprite = love.graphics.newImage("resources/sprites/bard.png")
    self.player = entities.Player(vector(spawn.x, spawn.y), pl_sprite)

    self.world:add(self.player)

    self.camera = camera(self.player.position.x, self.player.position.y)

end

function game:update(dt)
    if love.keyboard.isDown('a') then
        self.player.velocity.x = self.player.velocity.x -10
    end
    if love.keyboard.isDown('s') then
        self.player.velocity.x = self.player.velocity.x + 10
    end
    if love.keyboard.isDown('r') then
        self.player.velocity.y = self.player.velocity.y + 10
    end
    if love.keyboard.isDown('w') then
        self.player.velocity.y = self.player.velocity.y - 10
    end

    self.world:update(dt, tiny.rejectAll("isDraw"))
    self.world.map:update(dt)

    -- camera
    local pl_pos = self.player.position
    local c_pos_x = self.camera.x
    local c_pos_y = self.camera.y
    self.camera:move(pl_pos.x - c_pos_x, pl_pos.y - c_pos_y)
end

function game:draw()
    love.graphics.setColor(1,1,1)
    local offsetx, offsety = self.camera.x - 1920/self.scale/2, self.camera.y - 1080/self.scale/2
    self.world.map:draw(-offsetx, -offsety, self.scale,self.scale)

    if globalState.debug then
        self.world.map:bump_draw(self.world.bump, -offsetx, -offsety, self.scale,self.scale)
    end
end

function game:mousepressed(x, y, button, istouch)
    if button == 1 then
        self.player.attack.now = true
    elseif button == 2 then
        self.world:add(entities.Enemy(vector(x,y)))
    end
end

function game:mousemoved(x,y, dx, dy, istouch)
    self.player.direction = vector(x,y) - self.player.position
end

return setmetatable(
    {},
    {
        __call = function(...) return new(...) end,
    }
)