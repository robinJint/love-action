local ui = {
    world = nil,
    detail = nil,
    saved = {},
    saved_set = {},
    views = {},
    names = {},
}

local util = require 'util'
local nk = util.nk

function ui.register(es, name)
    local i = #ui.views + 1
    ui.views[i] = es
    ui.names[i] = name
    -- print(i, #ui.views)
end

function ui.register_world(w)
    ui.world = w
end

function ui.set_detail(w)
    ui.detail = w
end

function ui.save(s)
    local i = #ui.saved + 1
    ui.saved[i] = s
    ui.saved_set[s] = i
end

function ui.unsave(s)
    local i = ui.saved_set[s]

    if i then
        table.remove(ui.saved, i)
    end
end

function ui.is_saved(s)
    return ui.saved_set[s] and true
end

ui.default = setmetatable({}, {
    __index = function()
        return function(x, _nk, name)
            if type(name) ~= 'string' then name = tostring(name) end
            nk.label((name or 'unkown') .. ': ' ..tostring(x))
        end
    end,
    __call = function(_, item, nk, name)
        if type(item) == 'table' then
            -- print('find default for table',name,  item, item.ui)
        end
        if type(item) == 'table' and getmetatable(item) and getmetatable(item).__ui then 
            -- print('own ui', item.ui)
            getmetatable(item).__ui(item, nk, name)
        else
            ui.default[type(item)](item, nk, name)
        end       
    end,
})

function ui.default.table(item, nk, name)
    nk.tree(
        {'node', name or tostring(item), nil, 'expanded'},
        function()
            for k, x in pairs(item) do
                ui.default(x, nk, tostring(k))
            end
        end
    )
end

function ui.build()
    local c_x = 30
    local get_coords = function()
        local coords = {c_x, 100, 300, 900}
        c_x = c_x + 330
        return unpack(coords)
    end

    if ui.world then
        local x,y,w,h = get_coords()
        nk.windowBeginF(
            {'#entities', x,y,w,h,
                'border', 'title', 'movable', 'scalable', 'scrollbar'},
            function()
                if #ui.saved > 0 then
                    nk.tree(
                        {'node', 'saved', nil, 'expanded'},
                        function()
                            for _, x in ipairs(ui.saved) do
                                if nk.button(tostring(x)) then
                                    ui.detail = x
                                end
                            end
                        end
                    )
                end
                nk.layoutRow('dynamic', 30, 1)
                for n,x in ipairs(ui.world.entities) do
                    if nk.button(tostring(n) .. ' ' .. tostring(x)) then
                        ui.detail = x
                    end
                end
            end
        )
    end

    if ui.detail then
        local x,y,w,h = get_coords()
        nk.windowBeginF(
            {'#detail', x,y,w,h,
                'border', 'title', 'movable', 'scalable', 'scrollbar'},
            function()
                nk.layoutRow('dynamic', 30, 2)
                if ui.is_saved(ui.detail) then
                    if nk.button('unsave') then
                        ui.unsave(ui.detail)
                    end
                else 
                    if nk.button('save') then
                        ui.save(ui.detail)
                    end
                end
                nk.button('delete')
                nk.layoutRow('dynamic', 30, 1)

                if getmetatable(ui.detail) and getmetatable(ui.detail).__ui then
                    getmetatable(ui.detail).__ui(ui.detail, nk, 'detail view')
                end
                for n, x in pairs(ui.detail) do
                    ui.default(x, nk, n)
                end
            end
        )
    end

    -- for viewI, view in pairs(ui.views) do
    --     print('build')
    --     local viewName = ui.names[viewI] or tostring(viewI)
    --     nk.windowBeginF(
    --         {'#' .. viewName, 330 * (viewI - 1) + 30, 100, 300, 900,
    --             'border', 'title', 'movable', 'scalable', 'scrollbar'}, 
    --         function()
    --             ui.default(view, nk, viewName)
    --         end
    --     )
    -- end

end

return ui