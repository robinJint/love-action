local tiny = require 'tiny'
local util = require 'util'
local entities = require 'entities'
local vector = require 'vector'

local mod = {}

mod.movement = tiny.processingSystem()
mod.movement.filter = tiny.requireAll("position", "velocity")
function mod.movement:process(e, dt)
    e.position = e.position + e.velocity * dt

    -- e.position.x = util.clamp(e.position.x, 0, 1920)
    -- e.position.y = util.clamp(e.position.y, 0, 1080)

    if e.drag then
        e.velocity = e.velocity * e.drag^dt
    end
end

mod.attack = tiny.processingSystem()
mod.attack.filter = tiny.requireAll("position", "direction", "attack")
function mod.attack:process(e, dt)
    if e.attack.timeout <= 0 then
        if e.attack.now then
            self.world:add(entities.Bullet(e.position, e.direction:normalized() * e.attack.bulletSpeed))
            e.attack.timeout = e.attack.baseTimeout
        end
    else
        e.attack.timeout = e.attack.timeout - dt
        e.attack.now = false
    end
end

mod.collision = tiny.processingSystem()
mod.collision.filter = tiny.requireAll("position", "box")
function mod.collision:process(e, dt)
    local bump = self.world.bump
    local pos = e.position - e.box / 2

    local actualX, actualY, cols, len = bump:move(e, pos.x, pos.y)

    e.position = vector(actualX, actualY) + e.box /2
end 
function mod.collision:onAdd(e)
    local newPos = e.position - e.box / 2
    local x,y = newPos:unpack()
    self.world.bump:add(e, x, y, e.box.x, e.box.y)
end
function mod.collision:onRemove(e)
    self.world.bump:remove(e)
end

mod.sprite = tiny.processingSystem()
mod.sprite.filter = tiny.requireAll("position", "sprite")
function mod.sprite:onAdd(e)
    self.world.map.layers["sprites"].sprites[e] = true
end
function mod.sprite:onRemove(e)
    self.world.map.layers["sprites"].sprites[e] = nil
end

return mod