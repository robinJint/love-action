local vector = require 'vector'

local vector_mt = getmetatable(vector())
vector_mt.__ui = function(vec, nk, name)
    vec.x = nk.property('#' .. name .. ' dx', 0, vec.x, 1920, 10, 1)
    vec.y = nk.property('#' .. name .. ' dy', 0, vec.y, 1920, 10, 1)
end

local color_mt = {
    __ui = function(color, nk, name)
        nk.layoutRow('dynamic', 100, 1)
        local r,g,b = nk.colorPicker(unpack(color))
        nk.layoutRow('dynamic', 30, 1)
        color[1] = r
        color[2] = g
        color[3] = b
    end,
}

local c = {}
c.__index = c
c.__tostring = function(e) return e.type or 'entity' end

function c.new()
	return setmetatable({}, c)
end

function c.Color(c)
    return setmetatable(c or {1,1,1}, color_mt)
end

function c.Position(e, v)
    if v then
        assert(vector.isvector(v), 'v not a vector')
    else
        v = vector()
    end
    e.position = v
end

function c.Velocity(e, v)
    if v then
        assert(vector.isvector(v), 'v not a vector')
    else
        v = vector()
    end
    e.velocity = v
end

function c.Direction(e, v)
    if v then
        assert(vector.isvector(v), 'v not a vector')
    else
        v = vector()
    end
    e.direction = v
end

local defAttack = {
    baseTimeout = 1,
    timeout = 0,
    bulletSpeed = 500,
}
defAttack.__index = defAttack

function c.Attack(e, ...)
    e.attack = setmetatable({...}, defAttack)
end

function c.Sprite(e, image, properties)
    assert(image, "image cannot be nil")
    e.sprite = {
        image = image,
        r = 0,
        properties = properties,
        offset = vector(0,0)
    }
end

local defDrawData = {
    type = 'circle',
    r = '50',
    color = c.Color{1,1,1}
}
defDrawData.__index = defDrawData

function c.DrawData(e, vals)
    e.drawData = setmetatable(vals, defDrawData)
end

local defCollision = {
    type = 'none',
    collidesWith = {},
    action = 'remove'
}
defDrawData.__index = defDrawData


function c.Box(e, b)
    assert(b, "b is nil")
    e.box = b
end

-- the module
return setmetatable({
    new = c.new,
}, {
    __call = function(_, ...) return c.new(...) end,
    __index = c,
})