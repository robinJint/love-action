
local quitState = {}
quitState.__index = quitState
function quitState.new()
    return setmetatable({}, quitState)
end
function quitState:update(dt)
    print('last state, quiting')
    love.event.quit()
end

local state = {
    states = { quitState.new() }
}

local mod = {}

function mod.current()
    return state.states[#state.states]
end

function mod.push(newState)
    table.insert(state.states, newState)
    mod.load()
end

function mod.pop()
    return table.remove(state.states)
end

function mod.switch(newState)
    mod.pop()
    mod.push(newState)
end

local all_callbacks = {'draw', 'update', 'load'}

-- fetch event callbacks from love.handlers
for k in pairs(love.handlers) do
    table.insert(all_callbacks, k)
end

for _, f in ipairs(all_callbacks) do
    mod[f] = function(...)
        if mod.current()[f] then
            mod.current()[f](mod.current(), ...)
        end
    end
end

-- the module
return mod