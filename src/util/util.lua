local util = {}

function util.calculateWH(w, h, ratio)
    if h * ratio > w then
        return w, w/ratio
    else
        return h*ratio, h
    end
end

function util.clamp(n, low, high)
    assert(n and low and high, "nil val in clamp")
    return math.min(math.max(n, low), high)
end

local Queue = {}
Queue.__index = Queue
function Queue.new()
    return setmetatable({
        __first = 0,
        __last = 0,
    }, Queue)
end

function Queue:push(x)
    self[self.__last] = x
    self.__last = self.__last + 1
end

function Queue:pop()
    if not self:peek() then
        return nil
    end

    local item = self[self.__first]
    self.__first = self.__first + 1
    return item
end

function Queue:peek()
    if self.__first == self.__last then
        return nil
    else
        return self[self.__first]
    end
end

util.Queue = setmetatable({}, {
    __call = function(_) return Queue.new() end
})

return util