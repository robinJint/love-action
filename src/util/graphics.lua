local graphics = {}

function graphics.push(fn)
    love.graphics.push()
        fn()
    love.graphics.pop()
end

return graphics