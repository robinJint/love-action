local nuklear = require 'nuklear'
local nk = {}
nk.__index = nk

function nk.windowBeginF(args, fn)
    if nuklear.windowBegin(unpack(args)) then
        fn()
    end
    nuklear.windowEnd()
end

function nk.frameBeginF(fn)
    nuklear.frameBegin()
    fn()
	nuklear.frameEnd()
end

function nk.tree(opt, fn)
    if nuklear.treePush(unpack(opt)) then
        fn()
        nuklear.treePop()
    end
end

return setmetatable(nk, {__index = nuklear})