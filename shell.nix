# with import <nixpkgs> {};

with import (builtins.fetchTarball {
  # Descriptive name to make the store path easier to identify
  name = "nixos-18.09";
  # Commit hash for nixos-unstable as of 2018-09-12
  url = https://github.com/NixOS/nixpkgs/archive/18.09-beta.tar.gz;
  # Hash obtained using `nix-prefetch-url --unpack <url>`
  sha256 = "147xyn8brvkfgz1z3jbk13w00h6camnf6z0bz0r21g9pn1vv7sb0";
}) {};

stdenv.mkDerivation {
  name = "luajit-env";
  buildInputs = [
    luajit love_11

    # Example Additional Dependencies
    pkgconfig 
  ];

  # Set Environment Variables
  # LD_LIBRARY_PATH = "/run/opengl-driver/lib:${xorg.libX11}/lib:${xorg.libXcursor}/lib:${xorg.libXrandr}/lib:${xorg.libXi}/lib";
  LUA_DIR="${luajit}";
  # LUA_PATH="src:lib/*/";

  shellHook = ''
    for x in $(find $PWD/lib/ -maxdepth 1 -type d); do
      export LUA_PATH="$(printf '%s/?.lua;%s/?/init.lua' $x $x);$LUA_PATH"
    done
    export LUA_PATH="$PWD/src/?.lua;$PWD/src/?/init.lua;$LUA_PATH"
    # export LUA_PATH="$PWD/src/?.lua;$(find $PWD/lib/ -maxdepth 1 -type d | perl -pe "s#\n#/?.lua;#g");$LUA_PATH"
  '';

}
