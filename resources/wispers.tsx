<?xml version="1.0" encoding="UTF-8"?>
<tileset name="wispers" tilewidth="32" tileheight="32" tilecount="441" columns="21">
 <image source="sprites/wispers.png" width="672" height="701"/>
 <terraintypes>
  <terrain name="grass" tile="43"/>
  <terrain name="sand" tile="106"/>
  <terrain name="water" tile="196"/>
  <terrain name="stone" tile="280"/>
 </terraintypes>
 <tile id="6" terrain="0,0,0,"/>
 <tile id="7" terrain="0,0,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="32" height="20"/>
  </objectgroup>
 </tile>
 <tile id="8" terrain="0,0,,0"/>
 <tile id="9" terrain=",,,0"/>
 <tile id="10" terrain=",,0,"/>
 <tile id="27" terrain="0,,0,"/>
 <tile id="29" terrain=",0,,0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="19" height="32">
    <properties>
     <property name="collidable" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
 <tile id="30" terrain=",0,,"/>
 <tile id="31" terrain="0,,,"/>
 <tile id="43" terrain="0,0,0,0"/>
 <tile id="44" terrain="0,0,0,0"/>
 <tile id="48" terrain="0,,0,0"/>
 <tile id="49" terrain=",,0,0"/>
 <tile id="50" terrain=",0,0,0"/>
 <tile id="64" terrain="0,0,0,0"/>
 <tile id="65" terrain="0,0,0,0"/>
 <tile id="72" terrain="0,0,0,1"/>
 <tile id="73" terrain="0,0,1,0"/>
 <tile id="90" terrain="0,0,0,1"/>
 <tile id="91" terrain="0,0,1,1"/>
 <tile id="92" terrain="0,0,1,0"/>
 <tile id="93" terrain="0,1,0,0"/>
 <tile id="94" terrain="1,0,0,0"/>
 <tile id="106" terrain="1,1,1,1"/>
 <tile id="107" terrain="1,1,1,1"/>
 <tile id="108" terrain="1,1,1,1"/>
 <tile id="111" terrain="0,1,0,1"/>
 <tile id="113" terrain="1,0,1,0"/>
 <tile id="114" terrain="1,1,1,0"/>
 <tile id="115" terrain="1,1,0,1"/>
 <tile id="127" terrain="1,1,1,1"/>
 <tile id="128" terrain="1,1,1,1"/>
 <tile id="129" terrain="1,1,1,1"/>
 <tile id="132" terrain="0,1,0,0"/>
 <tile id="133" terrain="1,1,0,0"/>
 <tile id="134" terrain="1,0,0,0"/>
 <tile id="135" terrain="1,0,1,1"/>
 <tile id="136" terrain="0,1,1,1"/>
 <tile id="148" terrain="1,1,1,1"/>
 <tile id="149" terrain="1,1,1,1"/>
 <tile id="150" terrain="1,1,1,1"/>
 <tile id="174" terrain="0,0,0,2"/>
 <tile id="175" terrain="0,0,2,2"/>
 <tile id="176" terrain="0,0,2,0"/>
 <tile id="195" terrain="0,2,0,2"/>
 <tile id="196" terrain="2,2,2,2"/>
 <tile id="197" terrain="2,0,2,0"/>
 <tile id="198" terrain="0,2,2,2"/>
 <tile id="199" terrain="2,0,2,2"/>
 <tile id="216" terrain="0,2,0,0"/>
 <tile id="217" terrain="2,2,0,0"/>
 <tile id="218" terrain="2,0,0,0"/>
 <tile id="219" terrain="2,2,0,2"/>
 <tile id="220" terrain="2,2,2,0"/>
 <tile id="258" terrain="0,0,0,3"/>
 <tile id="259" terrain="0,0,3,3"/>
 <tile id="260" terrain="0,0,3,0"/>
 <tile id="262" terrain="3,0,3,0"/>
 <tile id="274" terrain="3,3,3,3"/>
 <tile id="275" terrain="3,3,3,3"/>
 <tile id="277" terrain="3,3,3,0"/>
 <tile id="278" terrain="3,3,0,3"/>
 <tile id="279" terrain="0,3,0,3"/>
 <tile id="280" terrain="3,3,3,3"/>
 <tile id="281" terrain="3,0,3,0"/>
 <tile id="282" terrain="3,3,3,0"/>
 <tile id="283" terrain="3,3,0,3"/>
 <tile id="295" terrain="3,3,3,3"/>
 <tile id="296" terrain="3,3,3,3"/>
 <tile id="298" terrain="3,0,3,3"/>
 <tile id="299" terrain="0,3,3,3"/>
 <tile id="300" terrain="0,3,0,0"/>
 <tile id="301" terrain="3,3,0,0"/>
 <tile id="302" terrain="3,0,0,0"/>
 <tile id="303" terrain="3,0,3,3"/>
 <tile id="304" terrain="0,3,3,3"/>
 <tile id="309">
  <properties>
   <property name="collidable" type="bool" value="false"/>
  </properties>
 </tile>
 <wangsets>
  <wangset name="grass" tile="-1">
   <wangcornercolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#00ff00" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#0000ff" tile="-1" probability="1"/>
   <wangtile tileid="43" wangid="0x20202020"/>
   <wangtile tileid="44" wangid="0x20202020"/>
   <wangtile tileid="64" wangid="0x20202020"/>
   <wangtile tileid="65" wangid="0x20202020"/>
   <wangtile tileid="113" wangid="0x10102020"/>
  </wangset>
 </wangsets>
</tileset>
