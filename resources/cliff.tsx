<?xml version="1.0" encoding="UTF-8"?>
<tileset name="cliff" tilewidth="32" tileheight="32" tilecount="240" columns="15">
 <image source="sprites/cliff.png" width="503" height="518"/>
 <tile id="61">
  <objectgroup draworder="index">
   <object id="1" x="19.75" y="0" width="12.375" height="32"/>
  </objectgroup>
 </tile>
 <tile id="78">
  <objectgroup draworder="index">
   <object id="1" x="0" y="4" width="32" height="28">
    <properties>
     <property name="collidable" type="bool" value="true"/>
    </properties>
   </object>
  </objectgroup>
 </tile>
</tileset>
