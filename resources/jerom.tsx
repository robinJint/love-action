<?xml version="1.0" encoding="UTF-8"?>
<tileset name="jerom" tilewidth="16" tileheight="16" tilecount="280" columns="10">
 <image source="sprites/jerom.png" width="160" height="448"/>
 <terraintypes>
  <terrain name="wall" tile="21"/>
  <terrain name="ground" tile="50"/>
 </terraintypes>
 <tile id="20" terrain=",,,0">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="21" terrain=",,0,0">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="22" terrain=",,0,">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="23" terrain="0,0,0,">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="24" terrain="0,0,,0">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="30" terrain=",0,,0">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="32" terrain="0,,0,">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="33" terrain="0,,0,0">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="34" terrain=",0,0,0">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="40" terrain=",0,,">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="41" terrain="0,0,,">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="42" terrain="0,,,">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="collidable" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="50" terrain="0,0,0,0"/>
 <wangsets>
  <wangset name="wall" tile="-1">
   <wangcornercolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#00ff00" tile="-1" probability="1"/>
   <wangtile tileid="20" wangid="0x10102010"/>
   <wangtile tileid="21" wangid="0x10202010"/>
   <wangtile tileid="22" wangid="0x10201010"/>
   <wangtile tileid="23" wangid="0x20201020"/>
   <wangtile tileid="24" wangid="0x20102020"/>
   <wangtile tileid="30" wangid="0x10102020"/>
   <wangtile tileid="32" wangid="0x20201010"/>
   <wangtile tileid="33" wangid="0x20202010"/>
   <wangtile tileid="34" wangid="0x10202020"/>
   <wangtile tileid="40" wangid="0x10101020"/>
   <wangtile tileid="42" wangid="0x20101010"/>
   <wangtile tileid="50" wangid="0x20202020"/>
   <wangtile tileid="51" wangid="0x10101010"/>
  </wangset>
  <wangset name="wall2" tile="-1">
   <wangcornercolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#00ff00" tile="-1" probability="1"/>
  </wangset>
 </wangsets>
</tileset>
